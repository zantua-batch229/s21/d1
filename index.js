let studentNumbers = ["2020-1923","2020-1924","2020-1925","2020-1926","2020-1927"];
// SYNTAX let/const arrayName = [valueA, ValueB...]

let grades = [98, 94, 89, 90];

let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];

// Possible use of array buy NOT RECOMMENDED
let mixedArr = [12, "Asus", null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

// Alternative way to write arrays

let myTasks = [
	"drink HTML",
	"eat JavaScript",
	"inhale CSS",
	"bake sass"
	];

// creating an arra with values from variables

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];

console.log(myTasks);
console.log(cities);

// [SECTION] - Length Property
// gets the total number of items

console.log(myTasks.length);
console.log(cities.length);

// length property can also be used with strings. some array methods and properties can also be used with strings (but not decrementation)

let fullName = "Jamie Oliver";
console.log(fullName.length); //displays 12

// length property can also set the total number ot items in an array

myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// using decrementation

cities.length--;
console.log(cities);

// Array length incrementation

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length--;
console.log(theBeatles);

/*
    - Accessing array elements is one of the more common tasks that we do with an array
    - This can be done through the use of array indexes
    - Each element in an array is associated with it's own index/number
    - In JavaScript, the first element is associated with the number 0 and increasing this number by 1 for every element
    - The reason an array starts with 0 is due to how the language is designed
- Array indexes actually refer to an address/location in the device's memory and how the information is stored
    - Example array location in memory
        Array address: 0x7ffe9472bad0
        Array[0] = 0x7ffe9472bad0
        Array[1] = 0x7ffe9472bad4
        Array[2] = 0x7ffe9472bad8
    - In the example above, the first element and the array itself points to the same memory location and therefore is at 0 elements away from the location of the array itself
- Syntax
        arrayName[index];
*/


console.log(grades[0]);
console.log(computerBrands[3]);

let lakersLegends = ["Kobe", "Shaq", "LeBron", "Magic", "Kareem"];
console.log(lakersLegends[lakersLegends.length-1]);

// save/store array item in variable
let currentLaker = lakersLegends[2];
console.log(currentLaker);

// re-assign array values using item indices

console.log("Array before re-assignment");
console.log(lakersLegends);
lakersLegends[2] = "Gasol";
console.log("Array after re-assignment");
console.log(lakersLegends);

// Adding item to an array
// using indeces, you can also add items into an array

let newArray = [];
console.log(newArray[0]);
newArray[5] = "asdff";
console.log(newArray);
console.log(newArray[0]);
console.log(newArray.length);

// adding an element to array at the last
newArray[newArray.length] = "Lockhart";
console.log(newArray);

// looping thru array

let numbers = [5,56,1,55,87];

for (let index = 0; index < numbers.length; index++){
	if (numbers[index] % 5 === 0) {
		console.log(numbers[index] + " is divisible by 5")
	} else {
		console.log(numbers[index] + " is not divisible by 5")
	}
}

// [SECTION] MULTI-DIMENSIONAL ARRAYS
// ARRAY INSIDE AN ARRAY

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];
console.log(chessBoard);

// accessing an elements of a multi-dimensional array

// [1] -> Column, [4] -> Row
console.log(chessBoard[1][4]); //e2

// show as table
console.table(chessBoard);

console.log("Pawn moves to: " + chessBoard[1][5]);
